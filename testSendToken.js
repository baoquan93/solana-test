const {Keypair, Transaction, clusterApiUrl} = require("@solana/web3.js");
const splToken = require("@solana/spl-token");
const web3 = require("@solana/web3.js");

// Address: 5d7us7o82bYsy69thKmnPH4EUPbSdPnSNnB47wm4jnbg
let FROM_WALLET_SECRET_KEY = Uint8Array.from([238, 134, 162, 6, 127, 229, 91, 150, 155, 25, 218, 179, 102, 218, 8, 208, 119, 136, 108, 84, 98, 15, 57, 160, 54, 106, 113, 124, 225, 134, 8, 124, 68, 175, 120, 188, 246, 39, 201, 107, 191, 160, 141, 192, 209, 253, 158, 139, 160, 243, 241, 55, 85, 196, 243, 19, 243, 8, 157, 12, 106, 3, 163, 239]);
let TO_WALLET_SECRET_KEY = Uint8Array.from([211, 27, 189, 241, 255, 252, 30, 124, 57, 149, 145, 216, 161, 252, 61, 135, 197, 155, 56, 72, 213, 244, 130, 104, 19, 131, 25, 151, 188, 39, 70, 160, 89, 93, 199, 152, 126, 41, 10, 101, 186, 36, 83, 16, 224, 239, 225, 59, 244, 66, 81, 148, 155, 226, 17, 206, 95, 170, 22, 210, 40, 130, 192, 105]);

(async () => {
    // Connect to cluster
    let connection = new web3.Connection(clusterApiUrl('devnet'));
    // Construct wallet keypairs
    let fromWallet = Keypair.fromSecretKey(FROM_WALLET_SECRET_KEY);
    console.log('solana public address: ' + fromWallet.publicKey.toBase58());
    let toWallet = Keypair.fromSecretKey(TO_WALLET_SECRET_KEY);
    // Construct my token class
    let myMint;
    let fromToken;
    setTimeout(async function () {
        myMint = new splToken.Token(connection,
            new web3.PublicKey("HcyaEVJRUSy8UDmkdx866nFbpRZN5ttnACfSrAgTGM7G"),
            splToken.TOKEN_PROGRAM_ID,
            fromWallet
        );
        console.log('mint public address: ' + myMint.publicKey.toBase58());
        fromToken = await myMint.getOrCreateAssociatedAccountInfo(
            fromWallet.publicKey
        )
        console.log('fromToken public address: ' + fromToken.address.toBase58());
        // await myMint.mintTo(fromToken.address, fromWallet.publicKey, [], 10000);

        let toToken = await myMint.getOrCreateAssociatedAccountInfo(
            toWallet.publicKey
        )
        console.log('toToken public address: ' + toToken.address.toBase58());

        // Add token transfer instructions to transaction
        let transaction = new Transaction();
        transaction
            .add(
                splToken.Token.createTransferInstruction(
                    splToken.TOKEN_PROGRAM_ID,
                    fromToken.address,
                    toToken.address,
                    fromWallet.publicKey,
                    [],
                    500
                )
            );
        // Sign transaction, broadcast, and confirm
        let signature = await web3.sendAndConfirmTransaction(
            connection,
            transaction,
            [fromWallet]
        );
        console.log("SIGNATURE", signature);
        console.log("SUCCESS");
    }, 200);
})();
