const {Keypair, Transaction, clusterApiUrl} = require("@solana/web3.js");
const splToken = require("@solana/spl-token");
const web3 = require("@solana/web3.js");

// Address: 5d7us7o82bYsy69thKmnPH4EUPbSdPnSNnB47wm4jnbg
let DEMO_WALLET_SECRET_KEY = Uint8Array.from([238, 134, 162, 6, 127, 229, 91, 150, 155, 25, 218, 179, 102, 218, 8, 208, 119, 136, 108, 84, 98, 15, 57, 160, 54, 106, 113, 124, 225, 134, 8, 124, 68, 175, 120, 188, 246, 39, 201, 107, 191, 160, 141, 192, 209, 253, 158, 139, 160, 243, 241, 55, 85, 196, 243, 19, 243, 8, 157, 12, 106, 3, 163, 239]);
(async () => {
    // Connect to cluster
    let connection = new web3.Connection(clusterApiUrl('devnet'));
    // Construct wallet keypairs
    let fromWallet = Keypair.fromSecretKey(DEMO_WALLET_SECRET_KEY);
    console.log('solana public address: ' + fromWallet.publicKey.toBase58());
    let toWallet = Keypair.generate();
    // Construct my token class
    let myMint;
    let fromToken;
    setTimeout(async function () {
        myMint = await splToken.Token.createMint(connection, fromWallet, fromWallet.publicKey, null, 2, splToken.TOKEN_PROGRAM_ID)
        console.log('mint public address: ' + myMint.publicKey.toBase58());
        fromToken = await myMint.getOrCreateAssociatedAccountInfo(
            fromWallet.publicKey
        )
        console.log('fromToken public address: ' + fromToken.address.toBase58());
        await myMint.mintTo(fromToken.address, fromWallet.publicKey, [], 10000);

        let toToken = await myMint.getOrCreateAssociatedAccountInfo(
            toWallet.publicKey
        )
        console.log('toToken public address: ' + toToken.address.toBase58());

        // Add token transfer instructions to transaction
        let transaction = new Transaction();
        transaction
            .add(
                splToken.Token.createTransferInstruction(
                    splToken.TOKEN_PROGRAM_ID,
                    fromToken.address,
                    toToken.address,
                    fromWallet.publicKey,
                    [],
                    0
                )
            );
        // Sign transaction, broadcast, and confirm
        let signature = await web3.sendAndConfirmTransaction(
            connection,
            transaction,
            [fromWallet]
        );
        console.log("SIGNATURE", signature);
        console.log("SUCCESS");
    }, 20000);


})();
